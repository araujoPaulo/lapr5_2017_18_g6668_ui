import { Component} from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Comentario } from '../../../../common/app/models/index';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../../../common/app/services/auth.service';
import { environment } from '../../../../common/environments/environment';
import { PresentationService } from '../../../../common/app/services/presentation.service';

@Component({
    selector: 'app-presentations-comment',
    templateUrl: './presentations-comment.component.html',
    styleUrls: ['./presentations-comment.component.scss']
})

export class PresentationsCommentComponent {
    id          : number;
    comentarios : Array<Comentario> = [];
    loading     : boolean = false;
    creating    : boolean = false;
    comment     : string = "";

    constructor(
        public bsModalRef: BsModalRef, 
        private http: HttpClient, 
        private toastr: ToastrService,
        public authService: AuthService,
        private presentationService: PresentationService
    ) { }
    
    loadComments() {
        this.loading = true;
        this.presentationService.getPresentationComments(this.id)
        .subscribe(
            comentarios => {
                this.comentarios = comentarios;
                this.loading = false;
            }, 
            err => this.handleError(err)
        );
    }

    addComment() {
        this.creating = true;
        this.presentationService.addComment(this.id, this.comment)
        .subscribe(
            response => {
                this.creating = false;
                this.toastr.success('Comentário adicionado com sucesso.', 'Sucesso');
                this.bsModalRef.hide();
            },
            err => this.handleError(err)
        );
    }

    isInvalid(field: any): boolean {
        return field.invalid && (field.dirty || field.touched);
    }

    private handleError(err) {
        this.loading = this.creating = false;
        this.toastr.error(err.error.message, 'Erro');
    }
    
}
