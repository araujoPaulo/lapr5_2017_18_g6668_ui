import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../../common/app/services/auth.service';
import * as moment from 'moment';
import {ToastrService} from 'ngx-toastr';
import {ChartService} from "../../../../common/app/services/chart.service";
import {PrescriptionService} from "../../../../common/app/services/prescription.service";
import {tr} from "ngx-bootstrap";


@Component({
    selector: 'app-home',
    templateUrl: 'home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    chart = {
        countPatients: {
            labels: [],
            data: [],
            loading: false
        }
    };

    patients: any;
    selectedPatient: any;
    bsRangeValue: Date[] = [new Date(new Date().getTime() - (60*60*24*7*1000)), new Date()];

    constructor(private chartService: ChartService,
                private prescriptionService: PrescriptionService,
                public authService: AuthService,
                private toastr: ToastrService) {
    }

    ngOnInit() {
        if (this.authService.isAuthenticated()) {
            this.loadCountDoctorReceiptsByPatient();

        }
    }

    public loadCountDoctorReceiptsByPatient() {
        this.chart.countPatients.loading = true;
        this.chartService.countDoctorReceiptsByPatient(this.bsRangeValue[0], this.bsRangeValue[1]).subscribe(result => {
            this.prepareReceiptsByPatientDataToBarChart(result);
            this.chart.countPatients.loading = false;
        }, error => {

            this.toastr.error(error.error.message, 'Erro');
            this.chart.countPatients.loading = false;
        });
    }

    private prepareReceiptsByPatientDataToBarChart(users: any){
        this.patients = users;

        this.selectedPatient = null;

        if(this.patients.length > 0){
            this.selectedPatient = users[0];
        }

        this.chart.countPatients.data = [];
        this.chart.countPatients.labels = [];

        users.map(user => {
            this.chart.countPatients.data.push(user.count);
            this.chart.countPatients.labels.push(user.name);
        });

    }

}
