import { Component} from '@angular/core';
import { Receita } from '../../../../../common/app/models';
import { AuthService } from '../../../../../common/app/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import {ReceiptService} from "../../../../../common/app/services/receipt.service";

@Component({
    selector: 'app-search-receipt',
    templateUrl: './search-receipt.component.html',
    styleUrls: ['./search-receipt.component.scss']
})

export class SearchReceiptComponent {
    id       : string;
    receita  : Receita;
    searched : boolean = false;
    loading  : boolean = false;
    
    constructor(
        private receiptService: ReceiptService,
        private toastr: ToastrService,
        public authService: AuthService
    ) { }

    search() {
        this.loading = true;
        this.receiptService.getDoctorReceiptById(this.id)
        .subscribe(
            response => {
                this.searched = true;
                this.loading = false;
                this.receita = response;
            },
            err => {
                this.searched = true;
                this.loading = false;
                this.receita = null;
            }
        );
    }
}
