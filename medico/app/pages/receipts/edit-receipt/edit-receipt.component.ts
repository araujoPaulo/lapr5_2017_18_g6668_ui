import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Farmaco, Receita, Medicamento, Prescricao, Apresentacao } from '../../../../../common/app/models';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { PrescriptionComponent } from '../../../dialogs/prescription/prescription.component';
import * as moment from 'moment';
import 'rxjs/add/observable/forkJoin';
import { environment } from '../../../../../common/environments/environment';
import {ReceiptService} from "../../../../../common/app/services/receipt.service";
import {DrugService} from "../../../../../common/app/services/drug.service";
import {PresentationService} from "../../../../../common/app/services/presentation.service";
import {MedicineService} from "../../../../../common/app/services/medicine.service";
import {PrescriptionService} from "../../../../../common/app/services/prescription.service";

@Component({
    selector: 'app-edit-receipt',
    templateUrl: './edit-receipt.component.html',
    styleUrls: ['./edit-receipt.component.scss']
})
export class EditReceiptComponent implements OnInit, OnDestroy {
    id             : string;
    receita        : Receita = new Receita();
    farmacos       : Array<Farmaco> = [];
    apresentacoes  : Array<Apresentacao> = [];
    medicamentos   : Array<Medicamento> = [];
    loading        : boolean = false;
    sub            : Subscription;              // The route subscription object to handle params received in the url
    modalSub       : Subscription;              // The modal subscription object that handles when the modal is dismissed
    bsModalRef     : BsModalRef;
    currentEditing : number = -1;
    constructor(
        private receiptService: ReceiptService,
        private drugService: DrugService,
        private presentationService: PresentationService,
        private medicineService: MedicineService,
        private prescriptionService: PrescriptionService,
        private activeRoute: ActivatedRoute, 
        private toastr: ToastrService,
        private router: Router,
        private modalService: BsModalService
    ) { }
    
    ngOnInit() : void {
        this.createHandler();
        
        this.sub = this.activeRoute.params.subscribe(params => {
            this.id = params['id'];
            
            this.populateForm();
        });
    }
    
    ngOnDestroy(): void {
        this.sub.unsubscribe();
        this.destroyHandler();
    }
    
    addPrescription() {
        this.currentEditing = -1;
        this.bsModalRef = this.setupModal();
    }
    
    editPrescription(number: number) {
        this.currentEditing = number;
        let p = this.receita.prescricoes[number];
        let ref = this.setupModal();
        
        // Cast the string back to Date object
        p.dataValidade = moment(p.dataValidade).toDate();
        
        // Update the fields in the modal
        for (let property of Object.keys(p)){
            if (ref.content.form.hasOwnProperty(property)) {
                ref.content.form[property] = p[property];
            }
        }
        
        // Add the id field, to allow the update in the api
        ref.content.form._id = p._id;
        
        this.bsModalRef = ref;
    }
    
    private setupModal() : BsModalRef {
        let ref = this.modalService.show(PrescriptionComponent, {class: 'modal-lg'});
        ref.content.farmacos = this.farmacos;
        ref.content.apresentacoes = this.apresentacoes;
        ref.content.medicamentos = this.medicamentos;
        return ref;
    }
    
    private createHandler() {
        this.modalSub = this.modalService.onHide.subscribe(reason => {
            if (reason || this.bsModalRef.content.canceled) { // Backdrop click
                return;
            }
            
            this.loading = true;
            let prescription = this.sanitize(this.bsModalRef.content.form);
            if (this.currentEditing !== -1) { // The user was editing a prescription
                // Update the prescription in the api
                this.prescriptionService.editPrescription(this.id, prescription).subscribe(
                    data => {
                        this.toastr.success('Prescrição atualizada com sucesso.', 'Sucesso');
                        this.receita.prescricoes[this.currentEditing] = data;
                        this.receita.updatedAt = moment().toDate();
                        this.loading = false;
                    },
                    err => this.handleError(err)
                );
            } else { // The user was creating a new prescription
                this.prescriptionService.createPrescription(this.id, prescription).subscribe(
                    data => {
                        this.toastr.success('Prescrição adicionada com sucesso.', 'Sucesso');
                        this.receita.prescricoes.push(data);
                        this.receita.updatedAt = moment().toDate();
                        this.loading = false;
                    },
                    err => this.handleError(err)
                );
            }
            
        });
    }

    private sanitize(data) {
        // Remove fields that are not supposed to be sent to the api
        if (this.currentEditing === -1) {
            delete data._id;
        }

        return data;
    }

    private destroyHandler() {
        this.modalSub.unsubscribe();
    }
    
    private populateForm() {
        this.loading = true;
        Observable.forkJoin(
            this.receiptService.getDoctorReceiptById(this.id),
            this.drugService.getDrugs(),
            this.presentationService.getPresentations(),
            this.medicineService.getMedicines()
        )
        .subscribe(response => {
            this.receita = response[0];
            this.farmacos = response[1];
            this.apresentacoes = response[2];
            this.medicamentos = response[3];
            this.loading = false;
        }, err => this.handleError(err));
    }
    
    private handleError(err: HttpErrorResponse) {
        if (err.status === 404) {
            this.toastr.error("A receita especificada não existe.", "Erro");
            this.router.navigate(['/receitas']);
            return;
        }

        if (this.loading) {
            this.loading = false;
        }

        this.toastr.error(err.error.message, 'Erro');
    }
}
