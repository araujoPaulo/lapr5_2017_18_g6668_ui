import { Component, OnInit, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Farmaco, Apresentacao, User, Medicamento } from '../../../../../common/app/models';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { PrescriptionComponent } from '../../../dialogs/prescription/prescription.component';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/observable/forkJoin';
import { AuthService } from '../../../../../common/app/services/auth.service';
import {ReceiptService} from "../../../../../common/app/services/receipt.service";
import {UserService} from "../../../../../common/app/services/user.service";
import {DrugService} from "../../../../../common/app/services/drug.service";
import {PresentationService} from "../../../../../common/app/services/presentation.service";
import {MedicineService} from "../../../../../common/app/services/medicine.service";

@Component({
    selector: 'app-create-receipts',
    templateUrl: './create-receipts.component.html',
    styleUrls: ['./create-receipts.component.scss']
})
export class CreateReceiptsComponent implements OnInit, OnDestroy {
    farmacos       : Array<Farmaco> = [];
    apresentacoes  : Array<Apresentacao> = [];
    medicamentos   : Array<Medicamento> = [];
    form           : { utente ?: User; prescricoes ?: Array<any>; } = { prescricoes: []};
    currentEditing : number = -1;
    utentes        : Array<User> = [];
    bsModalRef     : BsModalRef;
    loading        : boolean = false;
    modalSub       : Subscription;
    constructor(
        private receiptService: ReceiptService,
        private userService: UserService,
        private drugService: DrugService,
        private presentationService: PresentationService,
        private medicineService: MedicineService,
        private toastr: ToastrService, 
        private modalService: BsModalService,
        private router: Router,
        public auth: AuthService
    ) { }
    
    ngOnInit() {
        this.createHandler();

        this.populateForm();
    }

    ngOnDestroy() {
        this.destroyHandler();
    }
    
    addPrescription() {
        this.bsModalRef = this.setupModal();
    }
    
    editPrescription(number: number) {
        this.currentEditing = number;
        let p = this.form.prescricoes[number];
        // Launch the modal
        this.bsModalRef = this.setupModal();
        

        // Update the fields in the modal
        for (let property of Object.keys(p)){
            this.bsModalRef.content.form[property] = p[property];
        }
    }
    
    deletePrescription(number: number) {
        this.form.prescricoes.splice(number, 1);
    }
    
    create() {
        this.loading = true;
        // Remove properties that are not supposed to be sent
        this.form.prescricoes = this.form.prescricoes.map(p => {
            return p;
        });
        this.receiptService.createReceipt(this.form).subscribe(
            data => {
                this.loading = false;
                this.toastr.success('Receita criada com sucesso', 'Sucesso');
                this.router.navigate(['/']);
            },
            err => {
                this.loading = false;
                this.handleError(err);
            }
        );
    }

    private populateForm() {
        this.loading = true;
        Observable.forkJoin(
            this.userService.getPatients(),
            this.drugService.getDrugs(),
            this.presentationService.getPresentations(),
            this.medicineService.getMedicines()
        )
        .subscribe(response => {
            this.utentes = response[0];
            this.farmacos = response[1];
            this.apresentacoes = response[2];
            this.medicamentos = response[3];
            this.loading = false;
        }, err => {
            this.loading = false;
            this.handleError(err);
        });
    }
    
    private setupModal() : BsModalRef {
        let ref = this.modalService.show(PrescriptionComponent, {class: 'modal-lg'});
        ref.content.farmacos = this.farmacos;
        ref.content.apresentacoes = this.apresentacoes;
        ref.content.medicamentos = this.medicamentos;
        return ref;
    }
    
    private createHandler() {
        this.modalSub = this.modalService.onHide.subscribe(reason => {
            if (reason || this.bsModalRef.content.canceled) { // Backdrop click
                return;
            }
            
            let p = this.transformModalData(this.bsModalRef.content.form);
            // console.log('Modal hidden', p);
            if (this.currentEditing !== -1) { // The user was editing a prescription
                this.form.prescricoes[this.currentEditing] = p;
                this.currentEditing = -1;
            }else { // The user was creating a new prescription
                this.form.prescricoes.push(p);
            }
            
        });
    }

    private destroyHandler() {
        this.modalSub.unsubscribe();
    }
    
    private transformModalData(data: any) {
        let tmpApresentacao = this.apresentacoes.find(a => a.id === data.idApresentacao);
        let tmpMedicamento = this.medicamentos.find(m => m.id === data.idMedicamento);
        data.nomeFarmaco = this.farmacos.find(f => f.id === data.idFarmaco).nome;
        data.apresentacao = `${tmpApresentacao.forma}, ${tmpApresentacao.concentracao}, ${tmpApresentacao.quantidade}`;
        data.nomeMedicamento = tmpMedicamento ? tmpMedicamento.nome : '---';
        
        delete data._id;

        return data;
    }
    
    private handleError(err) {
        this.toastr.error(err.error.message, 'Erro');
    }
    
}
