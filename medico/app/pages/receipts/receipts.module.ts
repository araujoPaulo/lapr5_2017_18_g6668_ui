import { NgModule } from '@angular/core';

import { ReceiptsRoutingModule } from './receipts-routing.module';

import { ReceiptsComponent } from './receipts.component';
import { CreateReceiptsComponent } from './create-receipts/create-receipts.component';
import { EditReceiptComponent } from './edit-receipt/edit-receipt.component';
import { ReceiptDetailsComponent } from './receipt-details/receipt-details.component';
import { SearchReceiptComponent } from './search-receipt/search-receipt.component';

import { SharedModule } from '../../../../common/app/shared/shared.module';

@NgModule({
    imports: [ ReceiptsRoutingModule, SharedModule ],
    declarations: [
        ReceiptsComponent,
        CreateReceiptsComponent,
        EditReceiptComponent,
        ReceiptDetailsComponent,
        SearchReceiptComponent
    ]
})
export class ReceiptsModule { }
