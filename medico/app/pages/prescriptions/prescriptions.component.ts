import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { Prescricao } from '../../../../common/app/models';
import * as moment from 'moment';
import {PrescriptionService} from "../../../../common/app/services/prescription.service";

@Component({
    selector: 'app-prescriptions',
    templateUrl: './prescriptions.component.html',
    styleUrls: ['./prescriptions.component.scss']
})
export class PrescriptionsComponent implements OnInit {
    prescricoes : Array<Prescricao> = [];
    loading     : boolean           = false;
    bsConfig    : Partial<BsDatepickerConfig>;
    minDate     : moment.Moment = moment();
    searchValue : Date;
    constructor(
        private prescriptionService: PrescriptionService,
        private toastr: ToastrService,
        private modalService: BsModalService
    ) { 
        this.bsConfig = {showWeekNumbers: false, dateInputFormat: "DD/MM/YYYY"};
    }
    
    ngOnInit() {
        this.loadPrescriptions();
    }

    clearSearch() {
        this.searchValue = null;
    }

    search(event) {
        this.loadPrescriptions();
    }

    private loadPrescriptions() {
        this.loading = true;
        let search = this.searchValue ? moment(this.searchValue).format("YYYY-MM-DD") : "";
        this.prescriptionService.getPrescriptionNotDispatched(search).subscribe(
            data => {
                this.loading = false;
                this.prescricoes = data;
            },
            err => this.handleError(err)
        );
    }

    private handleError(err) {
        this.loading = false,
        this.toastr.error(err.error.message, 'Erro');
    }
    
}
