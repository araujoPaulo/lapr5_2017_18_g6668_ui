import { NgModule } from '@angular/core';

import { PrescriptionsRoutingModule } from './prescriptions-routing.module';

import { PrescriptionsComponent } from './prescriptions.component';

import { SharedModule } from '../../../../common/app/shared/shared.module';

@NgModule({
    imports: [ PrescriptionsRoutingModule, SharedModule ],
    declarations: [
        PrescriptionsComponent
    ]
})
export class PrescriptionsModule { }
