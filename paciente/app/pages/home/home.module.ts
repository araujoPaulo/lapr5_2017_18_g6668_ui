import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { HomeComponent } from './home.component';
import {UserMedicinesComponent} from "../../../../common/app/pages/user-medicines/user-medicines.component";

import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from '../../../../common/app/shared/shared.module';

@NgModule({
    imports: [ HomeRoutingModule, SharedModule, ChartsModule ],
    declarations: [
        HomeComponent, UserMedicinesComponent
    ]
})
export class HomeModule { }
