import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Apresentacao } from '../../../../common/app/models/Apresentacao';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { PresentationsCommentComponent } from '../../dialogs/presentations-comment/presentations-comment.component';
import { environment } from '../../../../common/environments/environment';
import { PresentationService } from '../../../../common/app/services/presentation.service';

@Component({
    selector: 'app-presentations',
    templateUrl: 'presentations.component.html'
})
export class PresentationsComponent implements OnInit { 
    bsModalRef    : BsModalRef;
    apresentacoes : Array<Apresentacao> = [];
    search        : string = "";
    loading       : boolean = false;
    constructor(
        private http: HttpClient, 
        private toastr: ToastrService,
        private modalService: BsModalService,
        private presentationService: PresentationService
    ) {}
    
    ngOnInit() {
        this.getPresentations();
    }
    
    getPresentations() {
        this.loading = true;
        this.presentationService.getPresentations()
        .subscribe(
            response => {
                this.loading = false;
                this.apresentacoes = response;
            },
            err => this.handleError(err)
        );
    }

    showCommentModal(id: number) {
        this.bsModalRef = this.modalService.show(PresentationsCommentComponent, {class: 'modal-lg'});
        this.bsModalRef.content.id = id;
        this.bsModalRef.content.loadComments();
    }

    private handleError(err) {
        if (this.loading) {
            this.loading = false;
        }
        this.toastr.error(err.error.message, 'Erro');
    }
}
