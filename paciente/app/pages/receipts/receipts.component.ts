import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { PrescriptionDetailsComponent } from '../../dialogs/prescription-details/prescription-details.component';
import { Prescricao, Receita } from '../../../../common/app/models';
import { AuthService } from '../../../../common/app/services/auth.service';
import {ReceiptService} from "../../../../common/app/services/receipt.service";


@Component({
    selector: 'app-receipts',
    templateUrl: 'receipts.component.html',
    styleUrls: ['./receipts.component.scss']
})  
export class ReceiptsComponent implements OnInit { 
    receitas: Array<Receita>;
    bsModalRef : BsModalRef;
    loading: boolean = false;
    
    constructor(
        private receiptService: ReceiptService,
        private toastr: ToastrService, 
        private modalService: BsModalService, 
        public authService: AuthService
    ) {}
    
    ngOnInit() {
        this.getReceipts();
    }
    
    getReceipts() {
        this.loading = true;
        this.receiptService.getPatientReceipts()
        .subscribe(
            response => {
                this.receitas = response;
                this.loading = false;
            },
            err => {
                this.toastr.error(err.error.message, 'Erro');
                this.loading = false;
            }
        );
    }
    
    showPrescription(prescription: Prescricao) {
        this.bsModalRef = this.modalService.show(PrescriptionDetailsComponent, {class: 'modal-lg'});
        this.bsModalRef.content.prescricoes = prescription;
    }
    
}
