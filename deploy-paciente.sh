#!/bin/bash

set -x

git pull
npm install
ng build --prod --app paciente --output-path /srv/angular/paciente/
