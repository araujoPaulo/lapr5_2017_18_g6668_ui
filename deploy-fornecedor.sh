#!/bin/bash

set -x

git pull
npm install
ng build --prod --app fornecedor --output-path /srv/angular/fornecedor/
