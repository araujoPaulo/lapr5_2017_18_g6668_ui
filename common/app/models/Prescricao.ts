import { Aviamento } from "./index";

export class Prescricao {
    _id             : string;
    quantidade      : number;
    dataValidade    : Date;
    idMedicamento   : number;
    idFarmaco       : number;
    idApresentacao  : number;
    posologia       : string;
    nomeMedicamento : string;
    forma           : string;
    embalagem       : string;
    concentracao    : string;
    nomeFarmaco     : string;
    notificado      : boolean = false;
    aviamentos      : Array<Aviamento> = [];
}
