import { Farmacia } from "./Farmacia";

export class Order {
    _id   ?: string;
    deliveryStatus : String;
    orderDate: Date;
    quantity: number;
    pharmacy : Farmacia;
    medicineId: number;
    deliveryDate : Date;

}
