import {Farmacia} from "./Farmacia";

export class Coordinates {
    ordem_entrega: Array<Farmacia>;
    percurso_total: Array<Array<string>>;
    tempos_entrega: Array<number>;
    distancia_total: number;
}