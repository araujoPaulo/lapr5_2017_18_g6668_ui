export class Farmacia {
    id   ?: string;
    name : string;
    lat  : number;
    long : number;
    delivery_time ?: string;
}