import { Order } from "./Order";

export class Delivery {
    id   ?: string;
    orders:Order[];
    deliveryData:Object[];
}
