import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {of} from "rxjs/observable/of";
import {COORDINATES} from "../mock/coordinates.mock";
import {Coordinates} from "../models/Coordinates";
import {Order} from "../models/Order";
import {Delivery} from "../models/Delivery";
import {HttpClient} from "@angular/common/http";
import { environment } from '../../environments/environment';

@Injectable()
export class SupplierService {

  constructor(private http: HttpClient) { }

  getCoordinates(): Observable<Coordinates>{
      return of(COORDINATES);
  }

  getAllOrders():Observable<Order[]>{
    return this.http.get<Order[]>(environment.apiFornedor + "/api/order");
  }

  getAllDeliveries():Observable<Delivery[]>{
    return this.http.get<Delivery[]>(environment.apiFornedor + "/api/delivery");
  }

  getDelivery(deliveryId: string): Observable<Delivery>{
    return this.http.get<Delivery>(environment.apiFornedor + `/api/delivery/${deliveryId}`);
  }
  
}
