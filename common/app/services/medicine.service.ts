import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Medicamento} from "../models";
import {environment} from "../../environments/environment";

@Injectable()
export class MedicineService {

  constructor(private http: HttpClient) { }

  getMedicines():Observable<Medicamento[]>{
      return this.http.get<Medicamento[]>(environment.apiGestaoReceitasKong + '/api/guest/medicamentos');
  }

  getMedicineById(MedicamentoId: string): Observable<Medicamento>{
    return this.http.get<Medicamento>(environment.apiGestaoReceitasKong + `/api/guest/medicamentos/${MedicamentoId}`);
  }

}
