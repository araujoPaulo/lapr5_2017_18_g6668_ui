import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {HttpClient} from "@angular/common/http";
import { environment } from '../../environments/environment';
import {Prescricao, Receita} from "../models";
import {AuthService} from "./auth.service";

@Injectable()
export class PharmacyService {

    constructor(private http: HttpClient,
                public auth: AuthService
    ) { }

    public getAllPharmacies(){
        return this.http.get(environment.apiFarmaciasKong + "/api/farmacia");
    }

    /*public createPharmacy(name: string, lat: string, long: string){
        return this.http.post(environment.apiFarmaciasKong + '/api/farmacia', {name: name, lat: lat, long: long});
    }
*/
    public getReceiptById(receitaId: string): Observable<Receita>{
        return this.http.get<Receita>(environment.apiFarmaciasKong + `/api/receita/${receitaId}`);
    }

    public dispatchPrescription(receitaId: string, prescricaoId: string, quantidade: number): Observable<Prescricao>{
        return this.http.put<Prescricao>(environment.apiFarmaciasKong + `/api/receitas/${receitaId}/prescricoes/${prescricaoId}/aviar`, {pharmacyId: this.auth.getFarmacia(), quantity: quantidade});
    }
    
}
  