import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Apresentacao} from "../models/Apresentacao";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs/Observable";
import { Comentario } from '../models/index';

@Injectable()
export class PresentationService {

    constructor(private http: HttpClient) { }

    getPresentations():Observable<Apresentacao[]>{
        return this.http.get<Apresentacao[]>(environment.apiGestaoReceitasKong + '/api/guest/apresentacoes');
    }

    getPresentationComments(presentationId: number,):Observable<Comentario[]>{
        return this.http.get<Comentario[]>(environment.apiGestaoReceitasKong + `/guest/apresentacoes/${presentationId}/comentarios`);
    }

    addComment(id: number, comment: string){
        return this.http.post<Comment>(environment.apiGestaoReceitasKong + `/api/apresentacoes/${id}/comentarios`, {texto: comment});
    }

}