import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Medicamento, Posologia} from "../models";
import {environment} from "../../environments/environment";

@Injectable()
export class PosologyService {

  constructor(private http: HttpClient) { }

  getPosologiesOfMedicine(idMedicamento: number):Observable<Posologia[]>{
      return this.http.get<Posologia[]>(environment.apiGestaoReceitasKong + `/guest/medicamentos/${idMedicamento}/posologias`);
  }

}