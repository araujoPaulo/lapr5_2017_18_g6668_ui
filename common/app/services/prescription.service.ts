import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Prescricao} from "../models";
import {Observable} from "rxjs/Observable";

@Injectable()
export class PrescriptionService {

  constructor(private http: HttpClient) { }

  editPrescription(receiptId: string, prescription: any):Observable<Prescricao>{
      return this.http.put<Prescricao>(environment.apiGestaoReceitasKong + `/api/receitas/${receiptId}/prescricoes/${prescription._id}`, prescription);
  }

  createPrescription(receiptId: string, prescription: any):Observable<Prescricao>{
      return this.http.post<Prescricao>(environment.apiGestaoReceitasKong + `/api/receitas/${receiptId}/prescricoes`, prescription);
  }

  dispatchPrescription(receiptId: string, prescriptionId: string, amount: number): Observable<Prescricao>{
      return this.http.put<Prescricao>(environment.apiGestaoReceitasKong + `/api/receitas/${receiptId}/prescricoes/${prescriptionId}/aviar`, {quantidade: amount});
  }


  getPrescriptionNotDispatched(timeStamp ?: string){
      return this.http.get<Prescricao[]>(environment.apiGestaoReceitasKong + `/api/utentes/prescricoes/poraviar`, {
          params: {
              data: timeStamp
          }
      })
  }

  getChartPrescriptions(){
    return this.http.get<any>(environment.apiGestaoReceitasKong + '/api/chart/prescricoes');
  }

  getPrescriptionNotDispatchedWithoutParams(){
        return this.http.get<Prescricao[]>(environment.apiGestaoReceitasKong + '/api/utentes/prescricoes/poraviar')
  }
}