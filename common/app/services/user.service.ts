import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { environment } from '../../environments/environment';
import {User} from "../models/User";


@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

    public login(email: String, password: String, role: String): Observable<any>{
        return this.http.post(environment.apiUtilizadoresKong + `/${role}/token`, {}, {
            headers: new HttpHeaders().set('Authorization', 'Basic ' + window.btoa(email + ':' + password)),
          });
    }

    public register(email: String, name: String, password: String, role: String): Observable<any>{
        return this.http.post(environment.apiUtilizadoresKong +'/getauth', { email: email, name: name, password: password, role: role });
    }

    public getPatients():Observable<User[]>{
        return this.http.get<User[]>(environment.apiUtilizadoresKong + '/utentes');
    }
    
    public sentTwoFactorAuthentication(email: String, password: String, role: String, code: string){
        return this.http.post(environment.apiUtilizadoresKong + `/twofactor`, {twoFactorCode: code}, {
            headers: new HttpHeaders().set('Authorization', 'Basic ' + window.btoa(email + ':' + password))
          });
    }

}
 