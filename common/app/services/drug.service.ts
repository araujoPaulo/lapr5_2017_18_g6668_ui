import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Farmaco} from "../models";
import {environment} from "../../environments/environment";

@Injectable()
export class DrugService {

  constructor(private http: HttpClient) { }

  getDrugs():Observable<Farmaco[]>{
      return this.http.get<Farmaco[]>(environment.apiGestaoReceitasKong + '/api/guest/farmacos');
  }

}
