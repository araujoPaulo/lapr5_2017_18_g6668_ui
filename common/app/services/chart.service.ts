import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {environment} from "../../environments/environment";
import {of} from "rxjs/observable/of";
import {COUNT_DOCTOR_RECEIPTS_BY_PATIENT} from "../mock/countDoctorReceiptsByPatient.mock";
import {COUNT_USER_MEDICINES} from "../mock/countUserMedicines";

@Injectable()
export class ChartService {

  constructor(private http: HttpClient) { }

  countDoctorReceiptsByPatient(startDate: Date, endDate: Date): Observable<any>{
      //return of(COUNT_DOCTOR_RECEIPTS_BY_PATIENT);
      return this.http.get<any>(environment.apiGestaoReceitasKong + `/api/chart/receitas/users?start_date=${startDate.toISOString()}&end_date=${endDate.toISOString()}`);
  }

  countUserMedicines(userId: string, startDate: Date, endDate: Date): Observable<any>{
      return this.http.get<any>(environment.apiGestaoReceitasKong + `/api/chart/receitas/${userId}/medicamentos?start_date=${startDate.toISOString()}&end_date=${endDate.toISOString()}`);
      //return of(COUNT_USER_MEDICINES);
  }

  getPrescriptions(): Observable<any>{
      return this.http.get<any>(environment.apiGestaoReceitasKong+ '/api/chart/prescricoes');
  }

}
