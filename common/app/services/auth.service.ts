import { Injectable } from '@angular/core';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { User } from '../models/User';

@Injectable()
export class AuthService {
    
    tokenName = 'token';

    constructor() { }
    
    public getToken(): string {
        return localStorage.getItem(this.tokenName);
    }

    public setToken(token: string): void {
        localStorage.setItem(this.tokenName, token);
    }

    public getQrKey(): string{
        return localStorage.getItem("qrKey");
    }

    public setQrKey(key: string): void{
        localStorage.setItem("qrKey", key);
    }

    public getEmail(): string{
        return localStorage.getItem("email");
    }

    public setEmail(email: string): void{
        localStorage.setItem("email", email);
    }

    public getPassword(): string{
        return localStorage.getItem("password");
    }

    public setPassword(password: string): void{
        localStorage.setItem("password", password);
    }

    public getFarmacia(): string{
        return localStorage.getItem("farmacia");
    }

    public setFarmacia(farmaciaId: string): void {
        localStorage.setItem("farmacia", farmaciaId);
    }

    public getCode(): string{
        return localStorage.getItem("twoFactorCode");
    }

    public setCode(twoFactorCode: string): void{
        localStorage.setItem("twoFactorCode", twoFactorCode);
    }

    public removeToken(): void {
        localStorage.removeItem(this.tokenName);
    }
    
    public isAuthenticated(): boolean {
        // return a boolean reflecting 
        // whether or not the token is expired
        return tokenNotExpired(this.tokenName);
    }

    public getUser(): User {
        let token = this.isAuthenticated() && this.getToken();
        
        if (!token) {
            return {id: "---", name: "Convidado", email: "", role: "guest"};
        }
        return JSON.parse(localStorage.getItem("user"));
    }

    public setUser(user: User): void{
        localStorage.setItem("user", JSON.stringify(user));
    }

    public hasRole(role: string[] | string): boolean {
        if (!this.isAuthenticated()) {
            return false;
        }

        let userRole = this.getUser().role;

        // Make sure the role variable is always an array
        if (typeof role === 'string') {
            role = [role];
        }
        // console.log(role, userRole);
        return role.indexOf(userRole) !== -1;
    }

}
