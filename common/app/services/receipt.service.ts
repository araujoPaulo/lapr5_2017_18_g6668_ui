import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Receita} from "../models";
import {Observable} from "rxjs/Observable";
import {environment} from "../../environments/environment";

@Injectable()
export class ReceiptService {

  constructor(private http: HttpClient) { }

  getDoctorReceipts(): Observable<Receita[]>{
      return this.http.get<Receita[]>(environment.apiGestaoReceitasKong + '/api/receitas/medico');
  }

  getPatientReceipts(): Observable<Receita[]>{
      return this.http.get<Receita[]>(environment.apiGestaoReceitasKong + '/api/receitas/utente');
  }

  createReceipt(reciept: any){
      return this.http.post<Receita>(environment.apiGestaoReceitasKong + '/api/receitas', reciept);
  }

  getDoctorReceiptById(id: string): Observable<Receita>{
      return this.http.get<Receita>(environment.apiGestaoReceitasKong + `/api/receitas/medico/${id}`);
  }

  /*getPharmaceuticalReceiptById(id: string): Observable<Receita>{
        return this.http.get<Receita>(environment.apiGestaoReceitasKong + `/api/receitas/farmaceutico/${id}`);
  }*/

  getPatientReceiptById(id: string){
      return this.http.get<Receita>(environment.apiGestaoReceitasKong + `/api/receitas/utente/${id}`);
  }

  getAllReceipts(){
      return this.http.get<Receita[]>(environment.apiGestaoReceitasKong + '/api/receitas/');
  }


}
