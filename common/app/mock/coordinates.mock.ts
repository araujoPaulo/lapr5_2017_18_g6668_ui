import {Coordinates} from "../models/Coordinates";

let coordinates = new Coordinates();

coordinates.ordem_entrega = [
    {"name": 'fornecedor', "lat": 41.1506096, "long": -8.6108667, "delivery_time": "morning"},
    {"name": "farmacia1", "lat": 41.1502687, "long": -8.6120989, "delivery_time": "morning"},
    {"name": "farmacia2", "lat": 41.1535027, "long": -8.6100569, "delivery_time": "morning"},
    {"name": "farmacia3", "lat": 41.1665034, "long": -8.6115866, "delivery_time": "morning"},
    {"name": "farmacia4", "lat": 41.1507370, "long": -8.6108828, "delivery_time": "morning"},
    {"name": "farmacia5", "lat": 41.1587505, "long": -8.6045353, "delivery_time": "morning"},
    {"name": "farmacia6", "lat": 41.1487987, "long": -8.6121351, "delivery_time": "morning"},
    {"name": "farmacia7", "lat": 41.1533416, "long": -8.6053689, "delivery_time": "morning"},
    {"name": "farmacia8", "lat": 41.1535380, "long": -8.6054379, "delivery_time": "morning"},
    {"name": "farmacia9", "lat": 41.1511130, "long": -8.6085693, "delivery_time": "morning"},
    {"name": "farmacia10", "lat": 41.1598906, "long": -8.6027699, "delivery_time": "morning"},
    {"name": "farmacia11", "lat": 41.1591673, "long": -8.6061404, "delivery_time": "morning"}
];

coordinates.percurso_total = [
    [
        "Rua Heróis e Mártires de Angola"
    ],
    [
        "Rua Heróis e Mártires de Angola",
        "Rua de Ricardo Jorge"
    ],
    [
        "Rua do Almada"
    ],
    [
        "Rua de Ramalho Ortigão",
        "Rua do Clube dos Fenianos",
        "Rua Heróis e Mártires de Angola",
        "Rua do Alferes Malheiro"
    ],
    [
        "Rua do Alferes Malheiro",
        "Rua da Trindade",
        "Rua de Camões"
    ],
    [
        "Rua de Gonçalo Cristóvão"
    ],
    [
        "Rua Sá da Bandeira"
    ],
    [
        "Rua Sá da Bandeira",
        "Rua de Gonçalo Cristóvão",
        "Rua de Camões",
        "Rua da Trindade",
        "Rua do Alferes Malheiro",
        "Rua do Bonjardim",
        "Rua João de Oliveira Ramos"
    ],
    [
        "Rua João de Oliveira Ramos"
    ],
    [
        "Rua João de Oliveira Ramos",
        "Rua Santa Catarina",
        "Rua de Latino Coelho"
    ],
    [
        "Rua de Latino Coelho",
        "Praça do Marquês de Pombal",
        "Rua de Costa Cabral",
        "Rua de Álvaro Castelões",
        "Rua do Bolama",
        "Rua de Faria Guimarães",
        "Rua António Cândido",
        "Rua de Antero de Quental",
        "Rua do Vale Formoso"
    ],
    [
        "Rua do Vale Formoso",
        "Rua de Antero de Quental",
        "Travessa de São Brás",
        "Rua de Camões",
        "Rua Heróis e Mártires de Angola"
    ]
];

coordinates.tempos_entrega = [
    0,
    0.0168,
    0.1668,
    0.36360000000000003,
    1.026,
    1.4532,
    2.0628,
    2.0904000000000003,
    4.4352,
    4.6056,
    5.1696,
    7.4244
];

coordinates.distancia_total = 8124;

export const COORDINATES: Coordinates = coordinates;