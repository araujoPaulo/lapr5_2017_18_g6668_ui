import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AuthService } from '../../../services/auth.service';
import {UserService} from "../../../services/user.service";
import { ToastrService } from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'paciente-two-factor-auth',
  templateUrl: './two-factor-auth.component.html',
  styleUrls: ['./two-factor-auth.component.scss']
})
export class TwoFactorAuthComponent implements OnInit {

  loading: boolean = false;
  a2f: string ='';
  
  constructor(public bsModalRef: BsModalRef,
              public authService: AuthService,
              private userService: UserService,
              private toastr: ToastrService, 
              private route: ActivatedRoute,
              private router: Router,
            ) { }

  ngOnInit() {
  }


  submitCode(){

    let role = this.route.snapshot.data['requiredRole'];

    this.userService.sentTwoFactorAuthentication(this.authService.getEmail(), this.authService.getPassword(), role, this.authService.getCode())
    .subscribe(
        (res: any) => {
          console.log(res);
            this.authService.setToken(res.token);
            this.authService.setUser(res.user);
            this.bsModalRef.hide();
            this.toastr.success('Foi autenticado com sucesso!');
            this.router.navigate(['/']);
            this.loading = false;
        },
        err => {
            this.toastr.error(err.error.message, 'Insira um código válido');
            this.loading = false;
        }
    );
    
  }
}
