import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {QRCodeComponent} from 'angular2-qrcode';
import { AuthService } from '../../../services/auth.service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'paciente-qr-code',
  templateUrl: './qr-code.component.html',
  styleUrls: ['./qr-code.component.scss']
})
export class QrCodeComponent implements OnInit {


  constructor(private router: Router,
              public bsModalRef: BsModalRef,
              public authService: AuthService,
              private toastr: ToastrService, 
            ) { }

  ngOnInit() {
  }

  redirect(){
    this.toastr.success('Conta criada com sucesso!');
    this.router.navigate(['/']);
  }

}

 
