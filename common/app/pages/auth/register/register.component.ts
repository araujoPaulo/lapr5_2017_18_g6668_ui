import { Component, TemplateRef } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from "../../../services/user.service";
import {AuthGuard} from "../../../guards/auth.guard";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { QrCodeComponent } from '../qr-code/qr-code.component';

@Component({
  selector: 'demo-modal-service-static',
  templateUrl: 'register.component.html'
})

export class RegisterComponent {

  errors: Array<string> = [];
  email: string = '';
  name: string = '';
  password: string = '';
  passwordConfirmation: string = '';
  terms: boolean = false;
  loading: boolean = false;
  modalRef: BsModalRef;

  constructor(private authService: AuthService,
              private guard: AuthGuard,
              private userService: UserService,
              private http: HttpClient,
              private toastr: ToastrService,
              private router: Router,
              private route: ActivatedRoute,
              private modalService: BsModalService) { }


  register() {
    this.loading = true;
    this.errors = [];

    if (this.password != this.passwordConfirmation) {
      this.errors.push('As Passwords não são iguais');
      this.loading = false;
      return;
    }

    if (!this.terms){
      this.errors.push('Tem de aceitar os termos e condições');
      this.loading = false;
      return;
    }

      let role = this.route.snapshot.data['requiredRole'];

      this.userService.register(this.email, this.name, this.password, role).subscribe(
        (res: any) => {
            this.authService.setQrKey(res.key.qr);
            this.modalService.show(QrCodeComponent, {class: 'modal-lg'});
            this.loading = false;
        },
        err => {
          this.toastr.error('Dados inválidos.');           
          this.loading = false;
        }
    );
  }

}
