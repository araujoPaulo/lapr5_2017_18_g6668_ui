import { Component, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {UserService} from "../../../services/user.service";
import {AuthService} from "../../../services/auth.service";
import {ActivatedRoute, Router} from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { TwoFactorAuthComponent } from '../two-factor-auth/two-factor-auth.component';


@Component({
    //selector: 'two-factor-auth',
    templateUrl: 'login.component.html'
})
export class LoginComponent {
    email: string = '';
    password: string = '';
    loading: boolean = false;
    modalRef: BsModalRef;
    rightCode: boolean = true;

    constructor(private userService: UserService, 
                private authService: AuthService, 
                private toastr: ToastrService, 
                private route: ActivatedRoute,
                private router: Router,
                private modalService: BsModalService
            ) { }
    

   /* openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    closeModal(){
        this.modalRef.hide();
    }*/

    login() {
        this.loading = true;

        let role = this.route.snapshot.data['requiredRole'];

        this.userService.login(this.email, this.password, role).subscribe(
            (res: any) => {
                this.authService.setEmail(this.email);
                this.authService.setPassword(this.password);
                this.modalService.show(TwoFactorAuthComponent, {class: 'modal-lg'});
            },
            err =>{
                this.toastr.error(err.error.message,'Ocorreu um erro');
                this.loading = false;
            }   
        );
    }
}
