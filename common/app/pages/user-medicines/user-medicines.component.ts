import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ChartService} from "../../services/chart.service";
import {ToastrService} from "ngx-toastr";

@Component({
    selector: 'user-medicines',
    templateUrl: './user-medicines.component.html',
    styleUrls: ['./user-medicines.component.scss']
})
export class UserMedicinesComponent implements OnChanges {

    chart = {
        countMedicnes: {
            labels: [],
            data: [],
            loading: false
        }
    };

    @Input()
    selectedPatient: any;

    @Input()
    bsRangeValue: Date[] = [new Date(new Date().getTime() - (60 * 60 * 24 * 7 * 1000)), new Date()];

    constructor(private chartService: ChartService,
                private toastr: ToastrService) {
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.loadCountUserMedicines();
    }

    public loadCountUserMedicines() {
        this.chart.countMedicnes.loading = true;
        this.chartService.countUserMedicines(this.selectedPatient.id, this.bsRangeValue[0], this.bsRangeValue[1]).subscribe(result => {

            this.chart.countMedicnes.data = [];
            this.chart.countMedicnes.labels = [];

            result.map(medicine => {
                this.chart.countMedicnes.data.push(medicine.count);
                this.chart.countMedicnes.labels.push(medicine._id[0]);
            });

            this.chart.countMedicnes.loading = false;
        }, error => {
            this.toastr.error(error.message);
            this.chart.countMedicnes.loading = false;
        });
    }

}
