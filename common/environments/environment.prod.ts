export const environment = {
  production: true,
  
  apiGestaoReceitasKong   : 'http://server-kong:8000/receitas',
  apiFarmaciasKong        : 'http://server-kongs:8000/farmacias',
  apiUtilizadoresKong     : 'http://server-kong:8000/utilizadores',
  socketUrl               : 'http://server-kong.ddns.net:8000'
};
