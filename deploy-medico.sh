#!/bin/bash

set -x

git pull
npm install
ng build --prod --app medico --output-path /srv/angular/medico/
