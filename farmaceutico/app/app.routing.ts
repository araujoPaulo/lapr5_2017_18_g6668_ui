import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';

// Guards
import { AuthGuard } from '../../common/app/guards/auth.guard';

import { P404Component } from '../../common/app/pages/404.component';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
    },
    {
        path: '',
        component: FullLayoutComponent,
        data: {
            title: 'Início'
        },
        children: [
            {
                path: 'home',
                loadChildren: './pages/home/home.module#HomeModule',
            },
            {
                path: 'receitas',
                canActivateChild: [AuthGuard],
                loadChildren: './pages/receipts/receipts.module#ReceiptsModule'
            },
            {
                path: 'apresentacoes',
                loadChildren: './pages/presentations/presentations.module#PresentationsModule',
            },
            {
                path: 'prescricoes',
                canActivate: [AuthGuard],
                data: {
                    requiredRole: 'farmaceutico'
                },
                loadChildren: './pages/prescriptions/prescriptions.module#PrescriptionsModule',
            },
        ]
    },
    {
        path: 'auth',
        component: SimpleLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: './pages/auth/auth.module#AuthModule',
                data: {
                    requiredRole: 'farmaceutico'
                },
            }
        ]
    },
    { path: 'not-found', component: P404Component },
    { path: '**', redirectTo: '/not-found' }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ],
    declarations: [P404Component]
})
export class AppRoutingModule {}
