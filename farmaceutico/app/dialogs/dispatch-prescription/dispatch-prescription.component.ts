import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Prescricao } from '../../../../common/app/models/index';
import { AuthService } from '../../../../common/app/services/auth.service';

@Component({
    selector: 'app-dispatch-prescription',
    templateUrl: './dispatch-prescription.component.html',
    styleUrls: ['./dispatch-prescription.component.scss']
})
export class DispatchPrescriptionComponent implements OnInit {
    prescricao: Prescricao = new Prescricao();
    canceled: boolean = true;
    form = {
        quantidade: 1
    };
    constructor(public bsModalRef: BsModalRef, public authService: AuthService) { }
    
    ngOnInit() {
    }
    
    dispatch() {
        // This method should only be triggered if it is possible to dispatch prescriptions
        // But for a sanity check, let's make sure it is actually valid
        if (this.prescricao.quantidade > 0 && this.authService.hasRole('farmaceutico')) {
            this.canceled = false;
        }
        this.bsModalRef.hide();
    }
}
