import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AuthService } from '../../../../common/app/services/auth.service';
import { PharmacyService } from '../../../../common/app/services/pharmacy.service';
import { Farmacia } from '../../../../common/app/models/Farmacia';

@Component({
  selector: 'farmaceutico-select-pharmacy',
  templateUrl: './select-pharmacy.component.html',
  styleUrls: ['./select-pharmacy.component.scss']
})
export class SelectPharmacyComponent implements OnInit {

  canceled: boolean = true;
  farmacias: Array<Farmacia> = [];
  

  constructor(public bsModalRef: BsModalRef, 
              public authService: AuthService, 
              public pharmacyService: PharmacyService
            ) { }

  ngOnInit() {
    this.getPharmacies();
  }

  getPharmacies() {

    this.pharmacyService.getAllPharmacies().subscribe(
        (res: any) => {
            this.farmacias = res;
        },
        err => {
            console.log("ERRO no get farmacias!!");
        }
    )
}

  select() {
    if (this.authService.hasRole('farmaceutico')) {
      this.canceled = false;
    }
    this.bsModalRef.hide();
  }
}
