import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../../../../common/app/services/auth.service';
import { Receita } from '../../../../common/app/models/index';
import { Prescricao } from '../../../../common/app/models/Prescricao';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../../common/environments/environment';
import {SelectPharmacyComponent} from "../../dialogs/select-pharmacy/select-pharmacy.component";
import {BsModalService} from "ngx-bootstrap/modal";
import { PrescriptionService } from '../../../../common/app/services/prescription.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit { 
    
    pies  = {
        prescricoes: {
            labels: ["Por aviar", "Parcialmente aviadas", "Completamente aviadas"],
            data: [0, 0, 0],
            loading: false
        }
    };
    constructor(
        private http: HttpClient,
        public authService: AuthService,
        private toastr: ToastrService,
        private modalService: BsModalService,
        private prescriptionService: PrescriptionService
    ) {}
    
    ngOnInit() {
        this.loadData();
        if (this.authService.isAuthenticated() && !this.authService.getFarmacia){
            this.modalService.show(SelectPharmacyComponent, {class: 'modal-lg'});
        }
    }
    
    loadData() {
        this.loadPrescriptions();
        
    }
    
    private loadPrescriptions() {
        this.pies.prescricoes.loading = true;
        this.prescriptionService.getChartPrescriptions().subscribe(
            response => {
                let data = [ response.por_aviar, response.parcial, response.aviado];
                // Need to replace the original array otherwise the chart wont update
                this.pies.prescricoes.data = data;
                this.pies.prescricoes.loading = false;
            },
            err => {
                this.toastr.error(err.error.message, 'Erro');
                this.pies.prescricoes.loading = false;
            }
        );
    }
}
