import { Component} from '@angular/core';
import { Receita } from '../../../../../common/app/models/index';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../../../../common/app/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../../../common/environments/environment';
import { ReceiptDetailsComponent } from '../receipt-details/receipt-details.component';
import { ReceiptService } from '../../../../../common/app/services/receipt.service';
import { PharmacyService } from '../../../../../common/app/services/pharmacy.service';

@Component({
    selector: 'app-search-receipt',
    templateUrl: './search-receipt.component.html',
    styleUrls: ['./search-receipt.component.scss']
})

export class SearchReceiptComponent {
    id       : string;
    receita  : Receita;
    searched : boolean = false;
    loading  : boolean = false;
    
    constructor(
        private http: HttpClient,
        private toastr: ToastrService,
        public authService: AuthService,
        private receiptService: ReceiptService,
        private pharmacySercice: PharmacyService

    ) { }
//this.receiptService.getPharmaceuticalReceiptById(this.id)
    search() {
        this.loading = true;
        this.pharmacySercice.getReceiptById(this.id) 
        .subscribe(
            response => {
                this.searched = true;
                this.loading = false;
                this.receita = response;
            },
            err => {
                this.searched = true;
                this.loading = false;
                this.receita = null;
            }
        );
    }
}
