import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { defineLocale } from 'ngx-bootstrap/bs-moment';
import { ptBr } from 'ngx-bootstrap/locale';
defineLocale('pt-br', ptBr);

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';

// Shared components
import { NAV_DROPDOWN_DIRECTIVES } from '../../common/app/shared/nav-dropdown.directive';
import { SIDEBAR_TOGGLE_DIRECTIVES } from '../../common/app/shared/sidebar.directive';
import { BreadcrumbsComponent } from '../../common/app/shared/breadcrumb.component';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';

// Http interceptors
import { TokenInterceptor } from '../../common/app/interceptors/token.interceptor';

// Services
import { AuthService } from '../../common/app/services/auth.service';
import { AuthGuard } from '../../common/app/guards/auth.guard';
import { WebSocketService } from '../../common/app/services/web-socket.service';
import {UserService} from '../../common/app/services/user.service';
import {ReceiptService} from "../../common/app/services/receipt.service";
import {DrugService} from "../../common/app/services/drug.service";
import {PresentationService} from "../../common/app/services/presentation.service";
import {MedicineService} from "../../common/app/services/medicine.service";
import {PrescriptionService} from "../../common/app/services/prescription.service";

// Dialogs
import { PrescriptionDetailsComponent } from './dialogs/prescription-details/prescription-details.component';
import { DispatchPrescriptionComponent } from './dialogs/dispatch-prescription/dispatch-prescription.component';
import { PresentationsCommentComponent } from './dialogs/presentations-comment/presentations-comment.component';
import { SelectPharmacyComponent } from './dialogs/select-pharmacy/select-pharmacy.component';
import { QrCodeComponent} from '../../common/app/pages/auth/qr-code/qr-code.component';

import { SharedModule } from '../../common/app/shared/shared.module';
import { PharmacyService } from '../../common/app/services/pharmacy.service';
import { TwoFactorAuthComponent } from '../../common/app/pages/auth/two-factor-auth/two-factor-auth.component';
import { QRCodeModule } from 'angular2-qrcode';


@NgModule({
    declarations: [
        AppComponent,
        FullLayoutComponent,
        SimpleLayoutComponent,
        NAV_DROPDOWN_DIRECTIVES,
        BreadcrumbsComponent,
        SIDEBAR_TOGGLE_DIRECTIVES,
        PrescriptionDetailsComponent,
        DispatchPrescriptionComponent,
        PresentationsCommentComponent,
        SelectPharmacyComponent,
        TwoFactorAuthComponent,
        QrCodeComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ToastrModule.forRoot({positionClass: 'toast-top-center'}),
        BrowserAnimationsModule,
        SharedModule,
        QRCodeModule
        ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        },
        AuthService,
        AuthGuard,
        WebSocketService,
        UserService,
        ReceiptService,
        DrugService,
        PresentationService,
        MedicineService,
        PrescriptionService,
        PharmacyService
    ],
    entryComponents: [
        PrescriptionDetailsComponent, 
        DispatchPrescriptionComponent, 
        PresentationsCommentComponent,
        SelectPharmacyComponent,
        TwoFactorAuthComponent, 
        QrCodeComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
