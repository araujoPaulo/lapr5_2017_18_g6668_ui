#!/bin/bash

set -x

git pull
npm install
ng build --prod --app farmaceutico --output-path /srv/angular/farmaceutico/
