import { Component, OnInit } from '@angular/core';
import { SupplierService } from "../../../../common/app/services/supplier.service";
import { Farmacia } from "../../../../common/app/models/Farmacia";
import { Delivery } from "../../../../common/app/models/Delivery";
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'fornecedor-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  delivery : Delivery;
  farmacias: any;
  loading: boolean = false;
  percurso_total: any;

  constructor(private supplierService: SupplierService, 
              private activeRoute : ActivatedRoute,
              private toastr : ToastrService) { }

  ngOnInit() {
     this.activeRoute.params.subscribe(params => {
        var delivery_id = params['id'];
        this.getDelivery(delivery_id);
    });
      
  }

  getDelivery(delivery_id) {
    this.loading = true;
    this.supplierService.getDelivery(delivery_id)
      .subscribe(
      response => {
        var delivery = response;
        this.delivery = delivery;
        this.farmacias = delivery.deliveryData["ordem_entrega"];
        this.percurso_total = delivery.deliveryData["percurso_total"];
        this.loading = false;
      },
      err => {
        this.toastr.error(err.error.message, 'Erro');
        this.loading = false;
      }
      );
  }

}
