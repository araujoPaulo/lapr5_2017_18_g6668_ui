import { Component, OnInit } from '@angular/core';
import { SupplierService } from "../../../../common/app/services/supplier.service";
import { Farmacia } from "../../../../common/app/models/Farmacia";
import { Delivery } from "../../../../common/app/models/Delivery";
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Order } from '../../../../common/app/models/Order';

@Component({
  selector: 'paciente-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.scss']
})
export class DeliveryComponent implements OnInit {

  loading = false;
  delivery : Delivery;
  farmacias: any;
  orders: Array<Order> = [];
  tempos_entrega : any;
  distancia_total : number;

  constructor(private supplierService: SupplierService, 
              private activeRoute : ActivatedRoute,
              private toastr : ToastrService) { }

  ngOnInit() {
    this.activeRoute.params.subscribe(params => {
      var delivery_id = params['id'];
      this.getDelivery(delivery_id);
    });
  }

  getDelivery(delivery_id) {
    this.loading = true;
    this.supplierService.getDelivery(delivery_id)
      .subscribe(
      response => {
        var delivery = response;
        this.delivery = delivery;
        this.farmacias = delivery.deliveryData["ordem_entrega"];
        this.tempos_entrega = delivery.deliveryData["tempos_entrega"];
        this.distancia_total = delivery.deliveryData["distancia_total"];
        this.orders = delivery.orders;
        this.loading = false;
      },
      err => {
        this.toastr.error(err.error.message, 'Erro');
        this.loading = false;
      }
      );
  }

}
