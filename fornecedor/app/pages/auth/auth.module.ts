import { NgModule } from '@angular/core';

import { LoginComponent } from '../../../../common/app/pages/auth/login/login.component';
import { RegisterComponent } from '../../../../common/app/pages/auth/register/register.component';

import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from '../../../../common/app/shared/shared.module';

@NgModule({
    imports: [ AuthRoutingModule, SharedModule ],
    declarations: [
        LoginComponent,
        RegisterComponent
    ]
})
export class AuthModule { }
