import { Component, OnInit } from '@angular/core';
import { SupplierService } from "../../../../common/app/services/supplier.service";
import { Order } from '../../../../common/app/models/Order';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'fornecedor-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
    orders: Array<Order> = [];
    loading: boolean = false;

    constructor(private supplierService: SupplierService,
        private toastr: ToastrService) { }

    ngOnInit() {
        this.getOrders();
    }

    getOrders() {
        this.loading = true;
        this.supplierService.getAllOrders()
            .subscribe(
            response => {
                this.orders = response;
                this.loading = false;
                console.log(this.orders);
            },
            err => {
                this.toastr.error(err.error.message, 'Erro');
                this.loading = false;
            }
            );
    }

}
