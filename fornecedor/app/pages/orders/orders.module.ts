import {NgModule} from '@angular/core';
import {SharedModule} from "../../../../common/app/shared/shared.module";
import {OrdersComponent} from "./orders.component";
import {OrdersRoutingModule} from "./orders-routing.module";
import {AgmCoreModule} from "@agm/core";

@NgModule({
    imports: [
        OrdersRoutingModule,
        SharedModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyDjvSfcktg_jRBY8D3MjGjfQQE5vSh4t_0'
        })
    ],
    declarations: [
        OrdersComponent,
    ]
})
export class OrdersModule {
}
