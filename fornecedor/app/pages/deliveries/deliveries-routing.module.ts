import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DeliveriesComponent} from "./deliveries.component";
import {MapComponent} from "../map/map.component";
import {DeliveryComponent} from "../delivery/delivery.component";


const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Entregas'
        },
        component: DeliveriesComponent
    }, 
    {
        path: ':id/map',
        component: MapComponent
    },
    {
        path: ':id',
        component: DeliveryComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DeliveriesRoutingModule {}
