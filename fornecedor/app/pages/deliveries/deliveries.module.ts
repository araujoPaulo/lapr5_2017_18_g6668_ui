import {NgModule} from '@angular/core';
import {SharedModule} from "../../../../common/app/shared/shared.module";
import {DeliveriesComponent} from "./deliveries.component";
import {DeliveriesRoutingModule} from "./deliveries-routing.module";
import {AgmCoreModule} from "@agm/core";
import {MapComponent} from "../map/map.component";
import {DeliveryComponent} from "../delivery/delivery.component";


@NgModule({
    imports: [
        DeliveriesRoutingModule,
        SharedModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyDjvSfcktg_jRBY8D3MjGjfQQE5vSh4t_0'
        })
    ],
    declarations: [
        DeliveriesComponent, 
        MapComponent,
        DeliveryComponent
    ]
})
export class DeliveriesModule {
}
