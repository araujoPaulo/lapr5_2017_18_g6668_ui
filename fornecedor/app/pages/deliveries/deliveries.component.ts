import { Component, OnInit } from '@angular/core';
import { SupplierService } from "../../../../common/app/services/supplier.service";
import { Order } from '../../../../common/app/models/Order';
import { Delivery } from '../../../../common/app/models/Delivery';
import { ToastrService } from 'ngx-toastr';
import { MapComponent } from '../map/map.component'
import { BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'fornecedor-deliveries',
  templateUrl: './deliveries.component.html',
  styleUrls: ['./deliveries.component.scss']
})
export class DeliveriesComponent implements OnInit {
  deliveries: Array<Delivery> = [];
  loading: boolean = false;

  constructor(private supplierService: SupplierService,
              private toastr: ToastrService,
              public modalService: BsModalService) { }

  ngOnInit() {
    this.getDeliveries();
  }

  getDeliveries() {
    this.loading = true;
    this.supplierService.getAllDeliveries()
      .subscribe(
      response => {
        this.deliveries = response;
        this.loading = false;
        //this.modalService.show(MapComponent, {class: 'modal-lg'});
      },
      err => {
        this.toastr.error(err.error.message, 'Erro');
        this.loading = false;
      }
      );
      
  }
}
