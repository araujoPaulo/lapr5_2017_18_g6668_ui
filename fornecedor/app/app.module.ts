import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { defineLocale } from 'ngx-bootstrap/bs-moment';
import { ptBr } from 'ngx-bootstrap/locale';
defineLocale('pt-br', ptBr);

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import {AgmCoreModule} from "@agm/core";

// Shared components
import { NAV_DROPDOWN_DIRECTIVES } from '../../common/app/shared/nav-dropdown.directive';
import { SIDEBAR_TOGGLE_DIRECTIVES } from '../../common/app/shared/sidebar.directive';
import { BreadcrumbsComponent } from '../../common/app/shared/breadcrumb.component';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';

// Http interceptors
import { TokenInterceptor } from '../../common/app/interceptors/token.interceptor';

// Services
import { AuthService } from '../../common/app/services/auth.service';
import { PharmacyService } from '../../common/app/services/pharmacy.service';
import { AuthGuard } from '../../common/app/guards/auth.guard';
import { WebSocketService } from '../../common/app/services/web-socket.service';

import { SharedModule } from '../../common/app/shared/shared.module';
import {UserService} from "../../common/app/services/user.service";
import {MapComponent} from "./pages/map/map.component";
import {SupplierService} from "../../common/app/services/supplier.service";
import { TwoFactorAuthComponent } from '../../common/app/pages/auth/two-factor-auth/two-factor-auth.component';
import { QRCodeModule, QRCodeComponent } from 'angular2-qrcode';
import { QrCodeComponent} from '../../common/app/pages/auth/qr-code/qr-code.component';

import { OrdersComponent } from './pages/orders/orders.component';
import { DeliveriesComponent } from './pages/deliveries/deliveries.component';
import { DeliveryComponent } from './pages/delivery/delivery.component';

@NgModule({
    declarations: [
        AppComponent,
        FullLayoutComponent,
        SimpleLayoutComponent,
        NAV_DROPDOWN_DIRECTIVES,
        BreadcrumbsComponent,
        SIDEBAR_TOGGLE_DIRECTIVES,
        TwoFactorAuthComponent,
        QrCodeComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ToastrModule.forRoot({positionClass: 'toast-top-center'}),
        BrowserAnimationsModule,
        SharedModule,
        QRCodeModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        },
        AuthService,
        PharmacyService,
        AuthGuard,
        WebSocketService,
        UserService,
        SupplierService
    ],
    entryComponents: [
       QRCodeComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
