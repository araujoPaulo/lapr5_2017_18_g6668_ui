import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';

// Guards
import { AuthGuard } from '../../common/app/guards/auth.guard';

import { P404Component } from '../../common/app/pages/404.component';
import {MapComponent} from "./pages/map/map.component";
import {OrdersComponent} from "./pages/orders/orders.component";
import {DeliveriesComponent} from "./pages/deliveries/deliveries.component";

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
    },
    {
        path: '',
        component: FullLayoutComponent,
        data: {
            title: 'Início'
        },
        children: [
            {
                path: 'home',
                loadChildren: './pages/home/home.module#HomeModule',
            }, {
                path: 'encomendas',
                loadChildren: './pages/orders/orders.module#OrdersModule',
            },
            {
                path: 'entregas',
                loadChildren: './pages/deliveries/deliveries.module#DeliveriesModule',
            }
        ]
    },
    {
        path: 'auth',
        component: SimpleLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: './pages/auth/auth.module#AuthModule',
                data: {
                    requiredRole: 'fornecedor'
                },
            }
        ]
    },
    { path: 'not-found', component: P404Component },
    { path: '**', redirectTo: '/not-found' }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ],
    declarations: [P404Component]
})
export class AppRoutingModule {}
